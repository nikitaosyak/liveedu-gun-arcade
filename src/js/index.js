import {Config} from './Config'
import {Renderer} from './Renderer'
import {Resources} from './Resources'
import {ScreenManager, SCREEN_TYPE} from './screen/ScreenManager'
import {UIFactory} from './game/UIFactory'
import {UserData} from './UserData'
import {URLParamUtil} from './utils/URLParamUtil'
import {Editor} from './editor/Editor'

window.onload = () => {

    const config = window.config = Config()
    if (config.isDev) { window.devEnv = {} }

    const resources = window.resources = Resources()
    const userData = window.USER_DATA = UserData()

    const startGame = () => {

        const editorMode = URLParamUtil.GET('editor')
        if (editorMode === 'true' && config.isDev) { Editor(); return }
        
        userData.connect().then(
        () => {
            if (config.isDev) { window.devEnv.stats = new Stats(); document.body.appendChild(window.devEnv.stats.dom) }

            const renderer = Renderer()
            const screens = ScreenManager(renderer)
            UIFactory.inject(screens)
            screens.transit(SCREEN_TYPE.MENU_MAIN)

            let time = Date.now()
            const gameLoop = () => {
                config.isDev && window.devEnv.stats.begin()
                
                let dt = (Date.now() - time) / 1000
                time = Date.now()

                screens.update(dt)
                renderer.update(dt)
                requestAnimationFrame(gameLoop)
                config.isDev && window.devEnv.stats.end()
            }

            requestAnimationFrame(gameLoop)
        },
        reason => {
            console.error(reason)
        })
    }

    resources.add('digest', 'assets/digest.json').load(() => {
        resources.getJSON('digest').assets.forEach(asset => {
            resources.add(asset.alias, asset.path)
        })
        resources.load(startGame)
    })
}