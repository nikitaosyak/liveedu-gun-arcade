export const MathUtil = {
    lerp: (v0, v1, t) => v0*(1-t)+v1*t
}