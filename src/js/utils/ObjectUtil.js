
export const PROGRESSION_KEYS = ['height', 'doveNumber', 'doveGap', 'angle', 'speed', 'size']

export const ObjectUtil = {
    copyKeys(fromObj, toObj, keys) {
        keys.forEach(key => {
            toObj[key] = fromObj[key]
        })
    }
}