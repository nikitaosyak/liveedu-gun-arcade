import {SCREEN_TYPE} from './ScreenManager'
import {ScreenBase} from './ScreenBase'
import {UIFactory} from '../game/UIFactory'

export class ScreenCustomization extends ScreenBase {

    constructor(owner) {
        super(owner, SCREEN_TYPE.MENU_CUSTOMIZATION)

        /*add content here*/
        this.add(UIFactory.makeAdoptableText(
            'Customize', 
            {x:0,y:0.5}, 
            {x:'left', xOffset: 50, y:'top', yOffset: 120}
        ))

        this.add(UIFactory.makeNavButton(
            'ui_return_right', 
            SCREEN_TYPE.MENU_MAIN, 
            {x:1,y:0.5},
            {x:'right', xOffset: 50, y:'top', yOffset: 120}
        ))
    }
}