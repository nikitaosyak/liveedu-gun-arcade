import {ScreenMainMenu} from './ScreenMainMenu'
import {ScreenGame} from './ScreenGame'
import {ScreenLeaderboards} from './ScreenLeaderboards'
import {ScreenCustomization} from './ScreenCustomization'

export const SCREEN_TYPE = {
    MENU_MAIN: 'MENU_MAIN',
    MENU_CUSTOMIZATION: 'MENU_CUSTOMIZATION',
    MENU_LEADERBOARDS: 'MENU_LEADERBOARDS',
    GAME: 'GAME'
}

export const ScreenManager = renderer => {

    let currentScreen = null

    const self = {
        get renderer() { return renderer },
        instantTransit: to => {
            if (currentScreen) {
                screens[currentScreen].hide()
            }
            screens[to].show()
            currentScreen = to
        },

        transit: to => {
            self.instantTransit(to)
        },

        update: dt => {
            Object.keys(screens).forEach(key => {
                screens[key].update(dt)
            })
        }
    }

    const screens = {
        [SCREEN_TYPE.MENU_MAIN] : new ScreenMainMenu(self),
        [SCREEN_TYPE.MENU_CUSTOMIZATION] : new ScreenCustomization(self),
        [SCREEN_TYPE.MENU_LEADERBOARDS] : new ScreenLeaderboards(self),
        [SCREEN_TYPE.GAME] : new ScreenGame(self),
    }
    if (window.config.isDev) self.getScreens = () => screens

    return self
}