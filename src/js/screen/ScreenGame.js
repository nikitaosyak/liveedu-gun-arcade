import {SCREEN_TYPE} from './ScreenManager'
import {ScreenBase} from './ScreenBase'
import {Background} from '../game/Background'
import {Weapon} from '../game/Weapon'
import {Input} from '../game/Input'
import {UIFactory} from '../game/UIFactory'
import {IInputPreventingElement} from '../behaviours/Base'
import {Game} from '../game/Game'

export class ScreenGame extends ScreenBase {

    constructor(owner) {
        super(owner, SCREEN_TYPE.GAME)

        this._retryUsed = false

        this._game = Game(this, owner.renderer)
        this._input = Input(owner.renderer.stage)
        this._input.on('fire', () => {
            this._game.shoot()
        })

        /*add content here*/
        this.add(Background())
        this.add(Weapon())

        this.scoreCounter = UIFactory.makeAdoptableText(
            'Score: 0', 
            {x:0, y:0}, 
            {x:'left', xOffset: 50, y:'top', yOffset: 50}, 50)
        this.add(this.scoreCounter)

        this.stageCouter = UIFactory.makeAdoptableText(
            'Stage: 0', 
            {x:1, y:0}, 
            {x:'right', xOffset: 50, y:'top', yOffset: 50}, 50)
        this.add(this.stageCouter)

        this._game.on('scoreChanged', value => {
            this.scoreCounter.visual.text = `Score: ${value}`
        })

        this._game.on('stageChanged', value => {
            this.stageCouter.visual.text = `Stage: ${value}`
        })

        const endSession = () => {
            window.alert(`you are dead. your score is: ${this._game.score}`)
            window.USER_DATA.addXp(this._game.score)
            window.USER_DATA.synchronize()
            window.USER_DATA.printCurrentState()
            owner.transit(SCREEN_TYPE.MENU_MAIN)
        }

        this._game.on('shotMissed', () => {
            if (window.config.isDev && window.devEnv.editor) { this._game.beginStage(); return; }

            if (this._retryUsed) {
                endSession()
            } else {
                if (window.confirm('watch ad to continue?')) {
                    this._game.beginStage()
                    this._retryUsed = true
                } else {
                    endSession()
                }
            }
        })

        if (window.config.isDev) this.getGame = () => this._game
    }

    show() {
        this._retryUsed = false
        this._game.resetProgress()
        this._game.beginStage()

        this._input.activate()

        this.scoreCounter.visual.text = `Score: 0`
        this.stageCouter.visual.text = `Stage: 0`
        super.show()
    }

    hide() {
        this._input.deactivate()
        super.hide()
    }

    update(dt) {
        if (!this._active) return
        this._game.update(dt)
    }
}