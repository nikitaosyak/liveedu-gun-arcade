import {SCREEN_TYPE} from './ScreenManager'
import {ScreenBase} from './ScreenBase'
import {UIFactory} from '../game/UIFactory'

export class ScreenMainMenu extends ScreenBase {

    constructor(owner) {
        super(owner, SCREEN_TYPE.MENU_MAIN)

        /*add content here*/
        this.add(UIFactory.makeAdoptableText(
            'GUN HIT', 
            {x:0.5,y:0}, 
            {x:'center', y:'top', yOffset: 60}
        ))

        this.add(UIFactory.makeRestartButton())

        this.add(UIFactory.makeNavButton(
            'ui_play', 
            SCREEN_TYPE.GAME, 
            {x:0.5,y:0.5},
            {x:'center', y:'middle', yOffset: -200},
            2
        ))

        this.add(UIFactory.makeNavButton(
            'ui_shop', 
            SCREEN_TYPE.MENU_CUSTOMIZATION, 
            {x:0,y:0},
            {x:'left', xOffset: 40, y:'middle', yOffset: 200}
        ))

        this.add(UIFactory.makeNavButton(
            'ui_leaderboards', 
            SCREEN_TYPE.MENU_LEADERBOARDS, 
            {x:1,y:0},
            {x:'right', xOffset: 40, y:'middle', yOffset: 200}
        ))

        this.levelCounter = UIFactory.makeAdoptableText(
            'Level: 0', 
            {x:0.5,y:1}, 
            {x:'center', y:'bottom', yOffset: 50}, 50)
        this.add(this.levelCounter)
    }

    show() {
        super.show()
        this.levelCounter.visual.text = `Level: ${window.USER_DATA.level}`
    }
}