
export class ScreenBase {

    constructor(owner, type) {
        this._owner = owner
        this._type = type

        this._active = false

        this._content = []
    }

    show() {
        this._active = true
        this._content.forEach(c => {
            this._owner.renderer.addObject(c)
        })
    }

    hide() {
        this._active = false
        this._content.forEach(c => {
            this._owner.renderer.removeObject(c)
        })
    }

    update(dt) {} // virtual method

    add(obj) {
        this._content.push(obj)

        if (!this._active) return
        this._owner.renderer.addObject(obj)
    }

    remove(obj) {
        const index = this._content.indexOf(obj)
        if (index < 0) {
            console.warn('There is no', obj, 'on the screen', this._type)
            return
        }
        this._content.splice(index, 1)

        if (!this._active) return
        this._owner.renderer.removeObject(obj)
    }
}