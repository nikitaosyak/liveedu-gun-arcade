
export const IVisual = (t, size, anchor, layer) => {
    const s = new PIXI.Sprite(window.resources.getTexture(t))
    s.width = size.x; s.height = size.y
    s.anchor.x = anchor.x; s.anchor.y = anchor.y

    return {
        get parent() { return layer },
        get visual() { return s }
    }
}

export const IText = (text, position, anchor, style, layer) => {
    const t = new PIXI.Text(text, new PIXI.TextStyle(style))
    t.x = position.x; t.y = position.y;
    t.anchor.x = anchor.x; t.anchor.y = anchor.y;

    return {
        get parent() { return layer },
        get visual() { return t }
    }
}

export const IButton = (visual, callback) => {
    visual.interactive = true
    visual.on('click', callback)
    visual.on('tap', callback)
}

export const IInputPreventingElement = (visual) => {
    visual.on('pointerdown', e => {
        e.stopPropagation()
    })
}