
export const IEmitter = eventLookup => {
    return {
        on: (event, callback) => {
            if (event in eventLookup) {
                eventLookup[event].push(callback)
            } else {
                eventLookup[event] = [callback]
            }
        },
        emit: (event, ...args) => {
            if (event in eventLookup) {
                eventLookup[event].forEach(cb => {
                    cb.apply(null, args)
                })
            }
        }
    }
}