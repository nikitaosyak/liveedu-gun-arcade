import {IEmitter} from '../behaviours/IEmitter'

export const DOMFactory = {

    _view: null,

    makeView: () => {
        const view = document.createElement('div')
        document.body.append(view)
        view.style['background-color'] = 'cadetblue'
        view.style.width = '50%'
        view.style.height = '100%'
        view.style['min-width'] = '200px'
        view.style['min-height'] = '500px'
        view.style.float = 'right'
        view.style.position = 'relative'

        DOMFactory._view = view

        return view
    },

    makeButton: (parent, size, text, onClick) => {
        const btn = document.createElement('button')
        btn.type = 'button'
        btn.innerHTML = text

        btn.addEventListener('click', _ => {
            onClick&&onClick()
        })

        parent.append(btn)

        return btn
    },

    makeSlider: (parent, text, value, range, onChange) => {
        const form = document.createElement('div')
        
        const label = document.createElement('label')
        label.innerHTML = text
        form.append(label)
        const slider = document.createElement('input')
        slider.type = 'range'
        slider.min = range.min
        slider.max = range.max
        slider.value = value
        form.append(slider)

        onChange&&slider.addEventListener('change', v => {
            onChange(Number.parseInt(slider.value))
        })

        onChange&&slider.addEventListener('input', v => {
            onChange(Number.parseInt(slider.value))
        })

        parent&&parent.append(form)

        form.getValue = () => Number.parseInt(slider.value)
        form.getMax = () => slider.max
        form.getMin = () => slider.min
        form.setValue = v => slider.value = v
        form.setMax = v => slider.max = v
        form.setMin = v => slider.min = v

        return form
    },

    makeImportButton: () => {
        return DOMFactory.makeButton(
            DOMFactory._view,
            {x: 100, y: 30},
            'Import',)
    },

    makeExportButton: (onClick) => {
        return DOMFactory.makeButton(
            DOMFactory._view,
            {x: 100, y: 30},
            'Export', onClick)
    },

    makeSpawnPointEditor: (name) => {
        const form = document.createElement('div')
        form.style.position = 'relative'
        form.style.left = '30px'

        const title = document.createElement('p')
        title.innerHTML = name
        title.style.margin = 0
        form.append(title)

        const heightSlider = DOMFactory.makeSlider(form, 'yOffset', 300, {min: 100, max: 800}, () => {
            self.emit('change')
        })

        const doveNumberSlider = DOMFactory.makeSlider(form, 'doves', 3, {min: 1, max: 6}, () => {
            self.emit('change')
        })

        const doveGapSlider = DOMFactory.makeSlider(form, 'doveGap', 100, {min: 80, max: 150}, () => {
            self.emit('change')
        })

        const angleSlider = DOMFactory.makeSlider(form, 'angle', 15, {min: 5, max: 60}, () => {
            self.emit('change')
        })

        const speedSlider = DOMFactory.makeSlider(form, 'speed', 150, {min: 80, max: 500}, () => {
            self.emit('change')
        })

        const sizeSlider = DOMFactory.makeSlider(form, 'size', 100, {min: 50, max: 150}, () => {
            self.emit('change')
        })

        const self = {
            get element() { return form },
            get height() { return heightSlider.getValue() },
            get doveNumber() { return doveNumberSlider.getValue() },
            get doveGap() { return doveGapSlider.getValue() },
            get angle() { return angleSlider.getValue() },
            get speed() { return speedSlider.getValue() },
            get size() { return sizeSlider.getValue() },

            set height(v) { heightSlider.setValue(v) },
            set doveNumber(v) { doveNumberSlider.setValue(v) },
            set doveGap(v) { doveGapSlider.setValue(v) },
            set angle(v) { angleSlider.setValue(v) },
            set speed(v) { speedSlider.setValue(v) },
            set size(v) { sizeSlider.setValue(v) },
        }
        Object.assign(self, IEmitter({}))
        return self
    }
}