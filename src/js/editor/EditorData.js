
export const EditorData = () => {

    let data = null

    const createData = () => {
        data = {}
        data.stages = []
    }

    const self = {
        get stages() { return data.stages },

        loadFromStorage: () => {
            const jsonData = JSON.parse(window.localStorage.gun_hit_editor || null)
            if (jsonData) {
                data = jsonData
            } else {
                createData()
            }
        },
        loadFromConfig: () => {
            return self.loadFromStorage()
        },
        saveToStorage: () => {
            window.localStorage.gun_hit_editor = JSON.stringify(data)
        },
        saveToConfig: () => {
            saveAs(
                new File([JSON.stringify(data)], 
                {type: "text/plain;charset=utf-8"}), "progression.json");
        }
    }
    return self
}