import {Renderer} from '../Renderer'
import {Resources} from '../Resources'
import {ScreenManager, SCREEN_TYPE} from '../screen/ScreenManager'
import {UIFactory} from '../game/UIFactory'
import {DOMFactory} from './DOMFactory'
import {EditorData} from './EditorData'
import {ObjectUtil, PROGRESSION_KEYS} from '../utils/ObjectUtil'

export const Editor = () => {

    window.devEnv.editor = true

    const editorData = window.EDITOR_DATA = EditorData()
    editorData.loadFromConfig()

    const renderer = Renderer()
    const screens = ScreenManager(renderer)
    UIFactory.inject(screens)
    screens.transit(SCREEN_TYPE.GAME)

    const gameScreen = screens.getScreens()[SCREEN_TYPE.GAME]
    const game = gameScreen.getGame()

    let time = Date.now()
    const gameLoop = () => {
        let dt = (Date.now() - time) / 1000
        time = Date.now()

        screens.update(dt)
        renderer.update(dt)
        requestAnimationFrame(gameLoop)
    }

    requestAnimationFrame(gameLoop)

    //
    //
    const view = DOMFactory.makeView()
    DOMFactory.makeImportButton()
    DOMFactory.makeExportButton(() => {
        editorData.saveToConfig()
    })

    const spEditors = [
        DOMFactory.makeSpawnPointEditor('left1'),
        DOMFactory.makeSpawnPointEditor('right1'),
        DOMFactory.makeSpawnPointEditor('left2'),
        DOMFactory.makeSpawnPointEditor('right2')
    ]
    const spawnPoints = game.getSpawnPoints()

    let currentPoints = 0
    const stageSlider = DOMFactory.makeSlider(view, 'stage', 0, {min: 0, max: 10}, stage => {
        currentPoints = 0
        if (editorData.stages[stage]) {
            const stageData = editorData.stages[stage]
            totalSpawnPointsSlider.setValue(stageData.spawnPoints)
            for (let i = 0; i < stageData.spawnPoints; i++) {
                ObjectUtil.copyKeys(
                    stageData.spawnPointsData[i], 
                    spEditors[i], PROGRESSION_KEYS)
            }

            updateEditors(stageData.spawnPoints)
        } else {
            updateEditors(1)
        }

        game.setProgress(stage)
    })



    const updateEditors = points => {
        console.log(points, currentPoints)
        if (currentPoints !== points) {
            spEditors.forEach(subEditor => {
                subEditor.element.parentElement && view.removeChild(subEditor.element)
            })

            spawnPoints.forEach(sp => sp.setActive(false))

            for (let i = 0; i < points; i++) {
                view.append(spEditors[i].element)
                const spawnPoint = spawnPoints[i]
                spawnPoint.setActive(true)
            }

            currentPoints = points
        }

        for (let i = 0; i < currentPoints; i++) {
            ObjectUtil.copyKeys(spEditors[i], spawnPoints[i], PROGRESSION_KEYS)
        } 

        editorData.stages[stageSlider.getValue()] = {
            spawnPoints: currentPoints,
            spawnPointsData: spEditors
                .filter((spe, i) => i < currentPoints)
                .map(spe => {
                    const spd = {}
                    ObjectUtil.copyKeys(spe, spd, PROGRESSION_KEYS)
                    return spd
                })
        }
        editorData.saveToStorage()
        // console.log(spawnPoints)

        game.beginStage()
    }
    const totalSpawnPointsSlider = DOMFactory.makeSlider(
        view, 'spawnPoints', 1, {min: 1, max: 4}, updateEditors)

    
    spEditors.forEach(subEditor => subEditor.on('change', () => { 
        updateEditors(totalSpawnPointsSlider.getValue()) 
    }))
    updateEditors(1)
}