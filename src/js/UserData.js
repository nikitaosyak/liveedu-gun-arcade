import {IEmitter} from './behaviours/IEmitter'

const STATE = {
    WAITING_TO_CONNECT:'WAITING_TO_CONNECT', 
    CONNECTING:'CONNECTNG', 
    CONNECTED:'CONNECTED',
    SAVING: 'SAVING',
}

export const UserData = () => {

    let state = STATE.WAITING_TO_CONNECT
    let data = {}

    const createData = () => {
        data.userid = 'DEADBEEF'
        data.xp = 0
        data.money = 0
        data.unlockedItems = []
        data.ownedItems = []
        data.currentSkin = null
    }

    const self = {
        /* debug methods */
        printCurrentState: () => JSON.stringify(data, null),
        loadState: jsonString => {
            data = JSON.parse(jsonString)
            self.synchronize().then(() => {
                window.location.href = window.location.origin
            })
        },
        resetProgress: () => {
            createData()
            self.synchronize().then(() => {
                window.location.reload(true)
            })
        },

        /* actual API */
        connect: () => {
            return new Promise((resolve, reject) => {
                if (state !== STATE.WAITING_TO_CONNECT) reject(`incorrect state: ${state}`)

                state = STATE.CONNECTNG

                const loadData = JSON.parse(window.localStorage.gun_hit || null)
                if (loadData === null) {
                    createData()
                } else {
                    data = loadData
                }

                state = STATE.CONNECTED
                console.log('UserData loaded: ', data)
                resolve()
            })
        },
        synchronize: () => {
            return new Promise((resolve, reject) => {
                if (state !== STATE.CONNECTED) reject(`incorrect state: ${state}`)

                state = STATE.SAVING

                window.localStorage.gun_hit = JSON.stringify(data)

                state = STATE.CONNECTED
                resolve()
            })
        },

        /* data access */
        get xp() { return data.xp },
        get level() { return Math.floor(data.xp/100) },
        get money() { return data.money },
        get unlockedItems() { return data.unlockedItems },
        get ownedItems() { return data.ownedItems },
        get currentSkin() { return data.currentSkin },

        /* data modifications */
        addXp: amount => {
            const prevLevel = self.level
            data.xp += amount
            if (self.level !== prevLevel) {
                self.emit('levelup')
            }
        },
        addMoney: amount => {
            data.money += amount
        },
        unlockItem: value => {
            const index = data.unlockedItems.indexOf(value)
            if (index > -1) {
                console.error(`cannot unlock item ${value}: already unlocked`)
                return
            }
            data.unlockedItems.push(value)
        },
        buyItem: value => {
            const index = data.ownedItems.indexOf(value)
            if (index > -1) {
                console.error(`cannot buy item ${value}: already owned`)
                return
            }
            data.ownedItems.push(value)
        },
        setCurrentSkin: value => {
            if (currentSkin === value) {
                console.warn(`applying the same skin ${value}`)
                return
            }
            data.currentSkin = value
        }
    }

    Object.assign(self, IEmitter({}))
    return self
}