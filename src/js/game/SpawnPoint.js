

export const SpawnPoint = (align, heightPoint, bounds) => {

    let savedBounds = bounds
    let active = false
    let doveNumber = 0
    let doveGap = 0
    let angle = 0

    return {
        get x() { return align === 'left' ? 0 : bounds.x },
        get y() { return heightPoint },
        get direction() { return align === 'left' ? 1 : -1 },
        get active() { return active },
        get doveNumber() { return doveNumber },
        get doveGap() { return doveGap },
        get angle() { return angle },

        setActive: value => { active = value },
        
        set height(value) { heightPoint = value },
        set doveNumber(value) { doveNumber = value },
        set doveGap(value) { doveGap = value },
        set angle(value) { angle = value }
    }
}