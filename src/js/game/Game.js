import {SpawnPoint} from './SpawnPoint'
import {ClayDove} from './ClayDove'
import {SCREEN_TYPE} from '../screen/ScreenManager'
import {IEmitter} from '../behaviours/IEmitter'
import {ObjectUtil, PROGRESSION_KEYS} from '../utils/ObjectUtil'

export const Game = (screen, renderer) => {

    const BASE_SPEED = 150

    let stage = 0
    let score = 0

    const scoreMultiplier = [-1, 1, 3, 10, 50, 500, 5000]
    const currentDoves = []

    const spawnPoints = [
        SpawnPoint('left', 350, renderer.currentViewport),
        SpawnPoint('right', 350, renderer.currentViewport),
        SpawnPoint('left', 530, renderer.currentViewport),
        SpawnPoint('right', 530, renderer.currentViewport)
    ]
    const getOppositeSpawn = idx => {
        if (idx === 0) return spawnPoints[1]
        if (idx === 1) return spawnPoints[0]
        if (idx === 2) return spawnPoints[3]
        if (idx === 3) return spawnPoints[2]
    }

    const self = {
        get score() { return score },
        beginStage: () => {
            // console.log('begin stage', stage)
            self.emit('stageChanged', stage)

            currentDoves.forEach(dove => {
                screen.remove(dove)
            })
            currentDoves.splice(0)

            const stageData = window.resources.getJSON('progression').stages[stage]
            // console.log(stageData)

            spawnPoints.forEach((point, spawnPointIndex) => {
                if (window.config.isDev && window.devEnv.editor) {
                    console.log('lalka')
                } else {
                    // console.log(stageData.spawnPoints-1, spawnPointIndex)
                    point.setActive(stageData.spawnPoints-1 >= spawnPointIndex)
                    if (!point.active) return
                    ObjectUtil.copyKeys(
                        stageData.spawnPointsData[spawnPointIndex], 
                        point, PROGRESSION_KEYS)    
                }
                if (!point.active) return
                for (let i = 0; i < point.doveNumber; i++) {
                    const dove = ClayDove(
                        spawnPointIndex, 
                        'saucer', 
                        point.speed, point.angle, point.direction, point.size)
                    dove.visual.x = point.x + Math.cos(dove.angle) * (point.doveGap * i)
                    dove.visual.y = point.y + Math.sin(dove.angle) * (point.doveGap * i)
                    currentDoves.push(dove)
                    screen.add(dove)
                }
            })
        },
        resetProgress: () => {
            stage = 0; score = 0;
        },
        shoot: () => {
            const center = renderer.currentViewport.x/2
            let hits = []
            currentDoves.forEach((dove, i) => {
                if (dove.left < center && dove.right > center) {
                    hits.push(i)
                }
            })
            if (hits.length > 0) {
                score += scoreMultiplier[hits.length]
                self.emit('scoreChanged', score)

                for (let i = hits.length-1; i >= 0; i--) {
                    const doveIndex = hits[i]
                    const dove = currentDoves.splice(doveIndex, 1)[0]
                    screen.remove(dove)
                }
            } else {
                self.emit('shotMissed')
            }

            if (currentDoves.length === 0) {
                if (window.config.isDev && window.devEnv.editor) { self.beginStage(); return; }
                stage += 1
                self.beginStage()
            }
        },
        update: dt => {
            currentDoves.forEach(dove => {
                dove.update(dt)

                let outOfBounds = false
                if (dove.left > renderer.currentViewport.x) {
                    // console.log(dove.spawnPoint)
                    outOfBounds = true
                }

                if (dove.right < 0) {
                    // console.log(dove.spawnPoint.x)
                    outOfBounds = true
                }

                if (outOfBounds) {
                    const newSpawn = getOppositeSpawn(dove.spawnPoint)
                    dove.direction = newSpawn.direction
                    dove.visual.x = newSpawn.x; dove.visual.y = spawnPoints[dove.spawnPoint].y   
                }
            })
        }
    }

    if (window.config.isDev) self.setProgress = _stage => { stage = _stage; self.beginStage() }
    if (window.config.isDev) self.getSpawnPoints = () => spawnPoints

    Object.assign(self, IEmitter({}))

    return self
}