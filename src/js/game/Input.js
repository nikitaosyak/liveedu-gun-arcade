import {IEmitter} from '../behaviours/IEmitter'

export const Input = stage => {

    let active = false

    const self = {
        activate: () => active = true,
        deactivate: () => active = false
    }

    const onTouchStart = () => {
        if (!active) return
        self.emit('fire')        
    }

    stage.on('pointerdown', onTouchStart)

    stage.interactive = true

    Object.assign(self, IEmitter({}))
    return self
}