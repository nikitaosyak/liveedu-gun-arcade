import {IVisual} from '../behaviours/Base'
import {RENDER_LAYER} from '../Renderer'

export const ClayDove = (spawnPoint, texture, speed, angle, direction, size) => {

    let started = Date.now()
    const timeout = 1000

    const self = {
        spawnPoint: spawnPoint,
        get left() { return self.visual.x-self.visual.width/2 },
        get right() { return self.visual.x+self.visual.width/2 },
        get top() { return self.visual.y-self.visual.height/2 },
        get bottom() { return self.visual.y+self.visual.height/2 },
        get angle() { return direction === 1 ? -(angle * Math.PI/180) : (angle * Math.PI/180) + Math.PI },

        set direction(v) {
            direction = v 
            self.visual.scale.x = Math.abs(self.visual.scale.x) * direction
        },

        update(dt) {
            if (Date.now() - started < timeout) return
            self.visual.x += Math.cos(self.angle) * speed*dt
            self.visual.y += Math.sin(self.angle) * speed*dt
        }
    }

    Object.assign(self, IVisual(
        texture, 
        {x:100 * 0.01 * size,y:100 * 0.01 * size}, 
        {x: 0.5, y: 0.5},
        RENDER_LAYER.GAME
    ))
    return self
}