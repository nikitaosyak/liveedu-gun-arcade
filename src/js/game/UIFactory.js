import {IVisual, IButton, IText} from '../behaviours/Base'
import {IAdoptable} from '../behaviours/Adoptable'
import {RENDER_LAYER} from '../Renderer'
import {SCREEN_TYPE} from '../screen/ScreenManager'

export const UIFactory = {

    inject: (screenMan) => {
        UIFactory.screenManager = screenMan
    },

    makeRestartButton: () => {
        const button = {}
        Object.assign(button, IVisual(
            'ui_restart', 
            {x:120,y:120}, 
            {x:1,y:0}, 
            RENDER_LAYER.UI
        ))

        Object.assign(button, IButton(button.visual, () => {
            window.USER_DATA.resetProgress()
        }))

        Object.assign(button, IAdoptable(button.visual, {x:'right',y:'top'}))

        return button
    },

    makeNavButton: (texture, screen, anchor, layoutRules, scale = 1) => {
        const button = {}
        Object.assign(button, 
            IVisual(
                texture, 
                {x: 140 * scale, y: 140 * scale}, 
                anchor, 
                RENDER_LAYER.UI
        ))

        Object.assign(button, IButton(button.visual, () => {
            UIFactory.screenManager.transit(screen)
        }))

        Object.assign(button, IAdoptable(
            button.visual, 
            layoutRules
        ))

        return button
    },

    makeAdoptableText: (text, anchor, pivotRules, fontSize=80) => {
        const t = {}
        Object.assign(t, IText(
            text, 
            {x:0,y:0}, 
            anchor, 
            {fontSize: fontSize, fill: '#444444'}, 
            RENDER_LAYER.UI
        ))

        Object.assign(t, IAdoptable(t.visual, pivotRules))

        return t
    }
}